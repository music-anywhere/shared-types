use super::samples_buffer::SamplesBuffer;
use serde::{Deserialize, Serialize};
use std::cmp::Ordering;
use std::time::SystemTime;

#[derive(Deserialize, Serialize, Eq)]
pub struct TimeTaggedSamplesBufferI16 {
    pub source: SamplesBuffer<i16>,
    pub start_time: SystemTime,
}

impl Ord for TimeTaggedSamplesBufferI16 {
    fn cmp(&self, other: &Self) -> Ordering {
        self.start_time.cmp(&other.start_time)
    }
}

impl PartialOrd for TimeTaggedSamplesBufferI16 {
    fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
        Some(self.cmp(other))
    }
}

impl PartialEq for TimeTaggedSamplesBufferI16 {
    fn eq(&self, other: &Self) -> bool {
        self.start_time == other.start_time
    }
}
