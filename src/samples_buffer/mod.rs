use rodio::Sample;
use rodio::Source;
use serde::{Deserialize, Serialize};
use std::convert::TryInto;
use std::mem;
use std::time::Duration;

#[derive(Deserialize, Serialize, PartialEq, Eq)]
pub struct SamplesBuffer<S>
    where
        S: Sample,
{
    channels: u16,
    sample_rate: u32,
    //in Hz
    data: Vec<S>,
    counter: usize,
    duration: Duration,
}

impl<S> SamplesBuffer<S>
    where
        S: Sample,
{
    //TODO: COMMENT
    pub fn new(channels: u16, sample_rate: u32, data: Vec<S>) -> Self {
        let counter = 0;

        let duration_ns = 1_000_000_000u64.checked_mul(data.len() as u64).unwrap()
            / sample_rate as u64
            / channels as u64;
        let duration = Duration::new(
            duration_ns / 1_000_000_000,
            (duration_ns % 1_000_000_000) as u32,
        );
        SamplesBuffer {
            channels,
            sample_rate,
            data,
            counter,
            duration,
        }
    }
    //TODO: COMMENT [---a---]split[---b---]
    //mantem o b no buffer self e devolve um buffer com o a
    pub fn remove_duration(&mut self, duration: Duration) -> SamplesBuffer<S> {
        let duration = duration.as_secs_f64();
        let number_of_samples = duration * (self.sample_rate * self.channels as u32) as f64;
        let number_of_samples = number_of_samples.floor() as u64;
        let mut data: Vec<S> = self.data.split_off(number_of_samples.try_into().unwrap());
        mem::swap(&mut data, &mut self.data);
        Self::new(self.channels, self.sample_rate, data)
    }
}

impl<S> Iterator for SamplesBuffer<S>
    where
        S: Sample,
{
    type Item = S;
    fn next(&mut self) -> Option<S> {
        if self.counter == self.data.len() {
            return None;
        }
        let result = *self.data.get(self.counter).unwrap();
        self.counter += 1;
        Some(result)
    }
}

impl<S> Source for SamplesBuffer<S>
    where
        S: Sample,
{
    fn current_frame_len(&self) -> Option<usize> {
        None
    }

    fn channels(&self) -> u16 {
        self.channels
    }

    fn sample_rate(&self) -> u32 {
        self.sample_rate
    }

    fn total_duration(&self) -> Option<Duration> {
        Some(self.duration)
    }
}
